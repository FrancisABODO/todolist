import React from "react";

function SearchBar({ todos, handleFilterTextChange }){
    /* const handleFilterTextChangeg = (e) => {
        console.log(e.target.value);
        console.log(todos);
        handleFilterTextChange(e.target.value);
    } */
    return(
        <input
            type="text"
            placeholder="Search..."
            //svalue={filterText} 
            onChange={(e) => handleFilterTextChange(e.target.value)}
        />
    );
}

export default SearchBar