import React from 'react';

// Import Components
import Todo from "./Todo"

const TodoList = ({ todos, setTodos, filteredTodos, setInputText, inputText, setIsUpdate }) => {
    console.log(todos);
    return(
        <div className="todo-container">
            <ul className="todo-list">
                {filteredTodos.map((todo) => (
                    <Todo 
                        todo={todo} 
                        setTodos={setTodos} 
                        todos={todos} 
                        key={todo.id}  
                        text={todo.text} 
                        setInputText={setInputText}
                        //filterText={filterText}
                        inputText={inputText}
                        setIsUpdate={setIsUpdate}
                    />
                ))}
            </ul>
        </div>
    );
}

export default TodoList