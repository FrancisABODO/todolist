import React from 'react'
import SearchBar from './SearchBar';

const Form = (props) => {

    const task = props.isUpdate;

    const inputTextHandler = (e) => {
        //console.log(e.target.value);
        props.setInputText(e.target.value);
    };
    const addTodo = () => {
        props.setTodos([
            ...props.todos, {
                text: props.inputText,
                completed: false,
                id: Math.random() * 1000
            },
        ]);
    }
    
    const updateTodo = () => {
        console.log(" task ", task);
        let taskUpdate = props.todos.map((item) => item.id === task.id ? { ...task, text : props.inputText} : item)
        console.log(" taskUpdate ", taskUpdate);
        props.setTodos(taskUpdate);
    }
    const submitTodoHandler = (e) => {
        e.preventDefault();
        if ( task ){
            updateTodo();
        } else {
            addTodo();
        }
        props.setInputText("");
    };
    const statusHandler = (e) => {
        console.log(e.target.value);
        props.setStatus(e.target.value);
    }
    return(
        <form>
            <input value={props.inputText} onChange={inputTextHandler} type="text" className="todo-input" />
            <button onClick={submitTodoHandler} className="todo-button" type="submit">
                <i className="fas fa-plus-square"></i>
            </button>
            <div className="select">
                <select onChange={statusHandler} name="todos"  className="filter-todo">
                    <option value="all">All</option>
                    <option value="completed">Completed</option>
                    <option value="uncompleted">uncompleted</option>
                </select>
            </div>
            <SearchBar
                filterText={props.filterText}
                isItExist={props.isItExist}
                handleFilterTextChange={props.handleFilterTextChange}
                todos={props.todos}
            />
        </form>
    );
}

export default Form