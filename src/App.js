/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import './App.css';

// Importing Component
import Form from './components/Form';
import TodoList from './components/TodoList'
import Todo from './components/Todo'

function App() {
  //State stuff
  const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);
  const [status,  setStatus] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);
  const [filterText, setFilterText] = useState("");
  const [isItExist, setIsItExcist] = useState(false);

  const [isUpdate, setIsUpdate] = useState();

  
  

  // RUN ONCE When the app start
  /* useEffect(() => {
    getLocalTodos();
  }, []); */

  // USE EFFECT
  useEffect(() => {
    //console.log("hey");
    filterHandler();
    //saveLocalTodos();
  }, [todos, status]);

  const filterHandler = () => {
    switch(status){
      case "completed":
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;
      case "uncompleted":
        setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  };

  // Save to Local
  /* const saveLocalTodos = () => {
    localStorage.setItem("todos", JSON.stringify(todos));
  };

  const getLocalTodos = () => {
    if(localStorage.getItem('todos') === null){
      localStorage.setItem('todos', JSON.stringify([]));
    } else {
      let todoLocal = JSON.parse(localStorage.getItem("todos"));
      setTodos(todoLocal);
    }
  }; */

  const handleFilterTextChange = (task, textSearchBar) => {
    let items = [];
    task.forEach((todo) => {
      //console.log(todo);
      if (todo.text.indexOf(textSearchBar) !== -1){
        console.log(" exemple " , todo);
        items.push(todo);
      } else if ( textSearchBar === " ") {
        items.push(task);
      }
    })
      return items;
  }
  console.log("actual state ",todos);
    

  return (
    <div className="App">
      <header>
        <h1>Gestionnaire de tâches</h1>
      </header>
      <Form 
        inputText={inputText}
        todos={todos} 
        setTodos={setTodos} 
        setInputText={setInputText} 
        setStatus={setStatus}
        filterText={filterText}
        //setFilteredTodos={setFilteredTodos}
        isItExist={isItExist}
        handleFilterTextChange={(value) => { 
          let newTodos = handleFilterTextChange(todos, value); 
          setFilteredTodos(newTodos);
        }}
        isUpdate={isUpdate}
      />
      <TodoList
        isItExist={isItExist}
        filterText={filterText}
        filteredTodos={filteredTodos} 
        setTodos={setTodos} 
        todos={todos}
        inputText={inputText}
        setInputText={(e) => setInputText(e)} 
        setIsUpdate={setIsUpdate}
      />
    </div>
  );
}

export default App;
